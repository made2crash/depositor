const assert = require('better-assert');
const async = require('async');
const bc = require('./src/bitcoin_client');
const db = require('./src/db');
const lib = require('./src/lib');
const fs = require('fs')


let client;

const depositAddresses = JSON.parse(fs.readFileSync('.addresses.json', 'utf8'));
assert(depositAddresses);

startBlockLoop();

function processTransactionIds(txids, callback) {
    bc.getTransactionIdsAddresses(txids, function(err, addressToAmountLists) {
        if (err) return callback(err);
        assert(txids.length === addressToAmountLists.length);
        var tasks = [];
        addressToAmountLists.forEach(function(addressToAmount, i) {
            var txid = txids[i];
            assert(txid);
            var usersToAmounts = {};
            Object.keys(addressToAmount).forEach(function(address) {
                var userId = depositAddresses[address];
                if (userId) {
                    usersToAmounts[userId] = addressToAmount[address];
                }
            });
            if (Object.keys(usersToAmounts).length > 0) {
                Object.keys(usersToAmounts).forEach(function(userId) {
                    tasks.push(function(callback) {
                        let toSend = usersToAmounts[userId];
                        if (toSend > 5) {
                            sendOutAmount = toSend - 5;
                            sendOut(sendOutAmount);
                            db.addDeposit(userId, txid, usersToAmounts[userId], callback);
                        } else {
                            console.log(`\n Can't send out deposits less than 5RVN \n`);
                            db.addDeposit(userId, txid, usersToAmounts[userId], callback);
                        }
                    });
                });
            }
        });
        async.parallelLimit(tasks, 3, callback);
    });
};

function sendOut(amountToSend) {
    withdrawalAddress = process.env.WITHDRAWAL_ADDRESS;
    bc.sendToAddress(withdrawalAddress, amountToSend, function (err, hash) {
        if (err) {
            console.log(err);
            if (err.message === 'Insufficient funds')
                return callback('PENDING');
            return callback('FUNDING_QUEUED');
        } else {
            console.log(`\n ${amountToSend}RVN was just sent out to the cold storage \n`);
            console.log(`\n This is the Hash : ${hash} gotten from sending out ${amountToSend}RVN \n`);
        }
    });
}

var lastBlockCount; 
var lastBlockHash;

function startBlockLoop() {
    db.getLastBlock(function (err, block) {
        if (err)
            throw new Error('Unable to get initial last block: ', err);

        lastBlockCount = block.height;
        lastBlockHash = block.hash;

        console.log('Initialized on block: ', lastBlockCount, ' with hash: ', lastBlockHash);

        blockLoop();
    });
}

function scheduleBlockLoop() {
    setTimeout(blockLoop, 20000);
}

function blockLoop() {
    bc.getBlockCount(function(err, num) {
        if (err) {
            console.log(err);
            console.error('Unable to get block count');
            return scheduleBlockLoop();
        }

        if (num === lastBlockCount) {
            console.log('BlockChain is on Block:', num);
            return scheduleBlockLoop();
        }

        bc.getBlockHash(lastBlockCount, function(err, hash) {
            if (err) {
                console.error('Could not get block hash, error: ' + err);
                return scheduleBlockLoop();
            }

            if (lastBlockHash !== hash) {
                db.getBlock(lastBlockCount - 1, function(err, block) {
                    if (err) {
                        console.error('ERROR: Unable jump back ', err);
                        return scheduleBlockLoop();
                    }

                    --lastBlockCount;
                    lastBlockHash = block.hash;
                    blockLoop();
                });
                return;
            }

            bc.getBlockHash(lastBlockCount+1, function(err, hash) {
                if (err) {
                    console.error('Unable to get block hash: ', lastBlockCount+1);
                    return scheduleBlockLoop();
                }

                processBlock(hash, function(err) {
                    if (err) {
                        console.error('Unable to process block: ', hash, ' because: ', err);
                        return scheduleBlockLoop();
                    }

                    ++lastBlockCount;
                    lastBlockHash = hash;

                    db.insertBlock(lastBlockCount, lastBlockHash, function(err) {
                       if (err)
                          console.error('Danger, unable to save results in database...');

                        blockLoop();
                    });
                });
            });
        });
    });
}

function processBlock(hash, callback) {
    var start = new Date();
    bc.getBlock(hash, function(err, blockInfo) {
        if (err) {
            console.error('Unable to get block info for: ', hash, ' got error: ', err);
            return callback(err);
        }
        var transactionsIds = blockInfo.tx;
        processTransactionIds(transactionsIds, function(err) {
            if (err) {
              console.log(err);
              console.log('Unable to process block (in ', (new Date() - start) / 1000, ' seconds)');
            }
            callback(err)
        });
    });
};