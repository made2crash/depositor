const debug = require('debug')('app:generate_addresses');
const lib = require('./lib');

let count = process.env.GENERATE_ADDRESSES || 0;

debug('Generating %n addresses', count);

console.log('{');

for (var i = 1; i <= count; ++i) {
  var address = lib.deriveAddress(i);
  console.log('"' + address + '": ' + i + (i != count ? ',' : ''));
}

console.log('}');