const bitcoinjs = require('bitcoinjs-lib');

const privKey = process.env.BIP32_PRIV;
const hdNode = bitcoinjs.HDNode.fromBase58(privKey, bitcoinjs.networks.ravencoin);

let count = process.env.GENERATE_ADDRESSES || 0;
let rescan = 'false';

for (var i = 1; i <= count; ++i) {
  if(i === count) {
    rescan = 'true'
  }
  console.log('raven-cli importprivkey ' +  hdNode.derive(i).keyPair.toWIF() + " '' " + rescan)
}